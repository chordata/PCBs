EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chordata
LIBS:005_hub-cache
EELAYER 25 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title "Chordata Hub Schematic"
Date "2019-01-24"
Rev "1.2"
Comp "Chordata"
Comment1 "Design by: Bruno Laurencich"
Comment2 "http://chordata.cc"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_02x04_Odd_Even J9
U 1 1 5B39048C
P 6350 4900
F 0 "J9" H 6400 5100 50  0000 C CNN
F 1 "HEADER-SBC" H 6400 4600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08_Pitch2.54mm" H 6350 4900 50  0001 C CNN
F 3 "" H 6350 4900 50  0001 C CNN
	1    6350 4900
	1    0    0    -1  
$EndComp
Text GLabel 6050 4800 0    55   Input ~ 0
sbc-3.3v
Text GLabel 6050 4900 0    55   Input ~ 0
ENABLE
Text GLabel 6750 5000 2    55   Input ~ 0
io_2
Text GLabel 6050 5100 0    55   Input ~ 0
io_3
Text GLabel 6750 4900 2    55   Input ~ 0
sbc-SDA
Text GLabel 6050 5000 0    55   Input ~ 0
sbc-SCL
Text GLabel 6750 5100 2    55   Input ~ 0
led-ctl
$Comp
L USB_A J11
U 1 1 5B3908BF
P 6300 6250
F 0 "J11" H 6100 6700 50  0000 L CNN
F 1 "OUT-5V" H 6100 6600 50  0000 L CNN
F 2 "Connectors:USB_A" H 6450 6200 50  0001 C CNN
F 3 "" H 6450 6200 50  0001 C CNN
	1    6300 6250
	1    0    0    -1  
$EndComp
$Comp
L USB_OTG J12
U 1 1 5B390B09
P 2150 6700
F 0 "J12" H 1950 7150 50  0000 L CNN
F 1 "IN-5V" H 1950 7050 50  0000 L CNN
F 2 "Connectors:USB_Micro-B" H 2300 6650 50  0001 C CNN
F 3 "" H 2300 6650 50  0001 C CNN
	1    2150 6700
	-1   0    0    1   
$EndComp
Text GLabel 1250 6900 0    55   Input ~ 0
PWR-5V
$Comp
L GND #PWR01
U 1 1 5B390F56
P 2500 7400
F 0 "#PWR01" H 2500 7150 50  0001 C CNN
F 1 "GND" H 2500 7250 50  0000 C CNN
F 2 "" H 2500 7400 50  0001 C CNN
F 3 "" H 2500 7400 50  0001 C CNN
	1    2500 7400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 5B391130
P 6450 6950
F 0 "#PWR02" H 6450 6700 50  0001 C CNN
F 1 "GND" H 6450 6800 50  0000 C CNN
F 2 "" H 6450 6950 50  0001 C CNN
F 3 "" H 6450 6950 50  0001 C CNN
	1    6450 6950
	1    0    0    -1  
$EndComp
Text GLabel 6600 6050 2    55   Input ~ 0
PWR-5V
$Comp
L MCP1603 U2
U 1 1 5B3B84C5
P 2100 8000
F 0 "U2" H 2000 8350 55  0000 C CNN
F 1 "MCP1603" H 2100 8250 55  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5_HandSoldering" H 2100 8000 55  0001 C CNN
F 3 "" H 2100 8000 55  0001 C CNN
	1    2100 8000
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5B3B885D
P 2000 9200
F 0 "#PWR03" H 2000 8950 50  0001 C CNN
F 1 "GND" H 2000 9050 50  0000 C CNN
F 2 "" H 2000 9200 50  0001 C CNN
F 3 "" H 2000 9200 50  0001 C CNN
	1    2000 9200
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 5B3B8BAA
P 1500 8400
F 0 "C1" H 1525 8500 50  0000 L CNN
F 1 "C" H 1525 8300 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 1538 8250 50  0001 C CNN
F 3 "" H 1500 8400 50  0001 C CNN
	1    1500 8400
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 5B3B8C01
P 3600 8400
F 0 "C2" H 3625 8500 50  0000 L CNN
F 1 "C" H 3625 8300 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3638 8250 50  0001 C CNN
F 3 "" H 3600 8400 50  0001 C CNN
	1    3600 8400
	1    0    0    -1  
$EndComp
$Comp
L L L1
U 1 1 5B3B8DFC
P 2850 8350
F 0 "L1" V 2800 8350 50  0000 C CNN
F 1 "L" V 2925 8350 50  0000 C CNN
F 2 "Inductors_SMD:L_Wuerth_WE-TPC-3816" H 2850 8350 50  0001 C CNN
F 3 "" H 2850 8350 50  0001 C CNN
	1    2850 8350
	0    1    1    0   
$EndComp
$Comp
L GND #PWR04
U 1 1 5B3B901A
P 1500 9200
F 0 "#PWR04" H 1500 8950 50  0001 C CNN
F 1 "GND" H 1500 9050 50  0000 C CNN
F 2 "" H 1500 9200 50  0001 C CNN
F 3 "" H 1500 9200 50  0001 C CNN
	1    1500 9200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5B3B903D
P 3600 8700
F 0 "#PWR05" H 3600 8450 50  0001 C CNN
F 1 "GND" H 3600 8550 50  0000 C CNN
F 2 "" H 3600 8700 50  0001 C CNN
F 3 "" H 3600 8700 50  0001 C CNN
	1    3600 8700
	1    0    0    -1  
$EndComp
$Comp
L Jumper_NC_Dual JP1
U 1 1 5B3B9293
P 4550 7000
F 0 "JP1" H 4600 6900 50  0000 L CNN
F 1 "pwr-sel" H 4550 7100 50  0000 C BNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 4550 7000 50  0001 C CNN
F 3 "" H 4550 7000 50  0001 C CNN
	1    4550 7000
	1    0    0    -1  
$EndComp
Text GLabel 5200 7000 2    55   Input ~ 0
sbc-3.3v
Text GLabel 5200 7650 2    55   Input ~ 0
VCC
$Comp
L PWR_FLAG #FLG06
U 1 1 5B3B97BD
P 5000 7500
F 0 "#FLG06" H 5000 7575 50  0001 C CNN
F 1 "PWR_FLAG" H 5000 7650 50  0000 C CNN
F 2 "" H 5000 7500 50  0001 C CNN
F 3 "" H 5000 7500 50  0001 C CNN
	1    5000 7500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5B3BA20C
P 7450 5250
F 0 "#PWR07" H 7450 5000 50  0001 C CNN
F 1 "GND" H 7450 5100 50  0000 C CNN
F 2 "" H 7450 5250 50  0001 C CNN
F 3 "" H 7450 5250 50  0001 C CNN
	1    7450 5250
	1    0    0    -1  
$EndComp
$Comp
L TEST TP7
U 1 1 5B3BAADC
P 7200 4600
F 0 "TP7" H 7200 4900 50  0000 C BNN
F 1 "TEST" H 7200 4850 50  0000 C CNN
F 2 "Test-points:TestPoint_Pad_D1.5mm" H 7200 4600 50  0001 C CNN
F 3 "" H 7200 4600 50  0001 C CNN
	1    7200 4600
	1    0    0    -1  
$EndComp
$Comp
L TEST TP10
U 1 1 5B3BABE0
P 1600 6900
F 0 "TP10" H 1600 7200 50  0000 C BNN
F 1 "T-BATT" H 1600 7150 50  0000 C CNN
F 2 "Test-points:TestPoint_Loop_D2.60mm_Drill1.6mm_Beaded" H 1600 6900 50  0001 C CNN
F 3 "" H 1600 6900 50  0001 C CNN
	1    1600 6900
	1    0    0    -1  
$EndComp
$Comp
L WS2812 U4
U 1 1 5B3BB196
P 6250 8500
F 0 "U4" H 6200 8700 55  0000 C CNN
F 1 "WS2812b" H 6250 8250 55  0000 C CNN
F 2 "LEDs:LED_WS2812B-PLCC4" H 6250 8450 55  0001 C CNN
F 3 "" H 6250 8450 55  0001 C CNN
	1    6250 8500
	0    -1   -1   0   
$EndComp
Text GLabel 7100 9100 2    55   Input ~ 0
led-ctl
$Comp
L GND #PWR08
U 1 1 5B3BB834
P 6150 9200
F 0 "#PWR08" H 6150 8950 50  0001 C CNN
F 1 "GND" H 6150 9050 50  0000 C CNN
F 2 "" H 6150 9200 50  0001 C CNN
F 3 "" H 6150 9200 50  0001 C CNN
	1    6150 9200
	1    0    0    -1  
$EndComp
Text GLabel 6500 7800 2    55   Input ~ 0
VCC
$Comp
L TEST TP11
U 1 1 5B3BCCE0
P 6900 9000
F 0 "TP11" H 6900 9300 50  0000 C BNN
F 1 "TEST" H 6900 9250 50  0000 C CNN
F 2 "Test-points:TestPoint_Plated_Hole_D2.0mm" H 6900 9000 50  0001 C CNN
F 3 "" H 6900 9000 50  0001 C CNN
	1    6900 9000
	1    0    0    -1  
$EndComp
Text GLabel 2800 5350 2    55   Input ~ 0
sbc-SDA
Text GLabel 2800 5450 2    55   Input ~ 0
sbc-SCL
$Comp
L TEST TP8
U 1 1 5B3BEEFC
P 2600 5350
F 0 "TP8" H 2600 5650 50  0000 C BNN
F 1 "TEST" H 2600 5600 50  0000 C CNN
F 2 "Test-points:TestPoint_Plated_Hole_D2.0mm" H 2600 5350 50  0001 C CNN
F 3 "" H 2600 5350 50  0001 C CNN
	1    2600 5350
	1    0    0    -1  
$EndComp
$Comp
L TEST TP9
U 1 1 5B3BEF80
P 2400 5450
F 0 "TP9" H 2400 5750 50  0000 C BNN
F 1 "TEST" H 2400 5700 50  0000 C CNN
F 2 "Test-points:TestPoint_Plated_Hole_D2.0mm" H 2400 5450 50  0001 C CNN
F 3 "" H 2400 5450 50  0001 C CNN
	1    2400 5450
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x06 J3
U 1 1 5B3BF547
P 1250 3150
F 0 "J3" H 1250 3450 50  0000 C CNN
F 1 "GATE_3" H 1250 2750 50  0000 C CNN
F 2 "Chordata:RJ12_6P_MOLEX" H 1250 3150 50  0001 C CNN
F 3 "" H 1250 3150 50  0001 C CNN
	1    1250 3150
	-1   0    0    -1  
$EndComp
Text GLabel 1850 3450 2    55   Input ~ 0
SDA_3
$Comp
L GND #PWR09
U 1 1 5B3C05D5
P 2250 4400
F 0 "#PWR09" H 2250 4150 50  0001 C CNN
F 1 "GND" H 2250 4250 50  0000 C CNN
F 2 "" H 2250 4400 50  0001 C CNN
F 3 "" H 2250 4400 50  0001 C CNN
	1    2250 4400
	1    0    0    -1  
$EndComp
Text GLabel 1450 2950 2    55   Input ~ 0
SCL_3
Text GLabel 1450 3150 2    55   Input ~ 0
ENABLE
Text GLabel 1450 3250 2    55   Input ~ 0
VCC
$Comp
L Conn_01x06 J2
U 1 1 5B3C1686
P 1250 2200
F 0 "J2" H 1250 2500 50  0000 C CNN
F 1 "GATE_2" H 1250 1800 50  0000 C CNN
F 2 "Chordata:RJ12_6P_MOLEX" H 1250 2200 50  0001 C CNN
F 3 "" H 1250 2200 50  0001 C CNN
	1    1250 2200
	-1   0    0    -1  
$EndComp
Text GLabel 1850 2500 2    55   Input ~ 0
SDA_2
Text GLabel 1450 2000 2    55   Input ~ 0
SCL_2
Text GLabel 1450 2200 2    55   Input ~ 0
ENABLE
Text GLabel 1450 2300 2    55   Input ~ 0
VCC
$Comp
L Conn_01x06 J1
U 1 1 5B3C18FC
P 1250 1250
F 0 "J1" H 1250 1550 50  0000 C CNN
F 1 "GATE_1" H 1250 850 50  0000 C CNN
F 2 "Chordata:RJ12_6P_MOLEX" H 1250 1250 50  0001 C CNN
F 3 "" H 1250 1250 50  0001 C CNN
	1    1250 1250
	-1   0    0    -1  
$EndComp
Text GLabel 1850 1550 2    55   Input ~ 0
SDA_1
Text GLabel 1450 1050 2    55   Input ~ 0
SCL_1
Text GLabel 1450 1250 2    55   Input ~ 0
ENABLE
Text GLabel 1450 1350 2    55   Input ~ 0
VCC
$Comp
L Conn_01x06 J4
U 1 1 5B3C3B85
P 3350 1600
F 0 "J4" H 3350 1900 50  0000 C CNN
F 1 "GATE_4" H 3350 1200 50  0000 C CNN
F 2 "Chordata:RJ12_6P_MOLEX" H 3350 1600 50  0001 C CNN
F 3 "" H 3350 1600 50  0001 C CNN
	1    3350 1600
	1    0    0    1   
$EndComp
Text GLabel 2700 1300 0    55   Input ~ 0
SDA_4
Text GLabel 3150 1800 0    55   Input ~ 0
SCL_4
Text GLabel 3150 1600 0    55   Input ~ 0
ENABLE
Text GLabel 3150 1500 0    55   Input ~ 0
VCC
$Comp
L Conn_01x06 J5
U 1 1 5B3C3B91
P 3350 2550
F 0 "J5" H 3350 2850 50  0000 C CNN
F 1 "GATE_5" H 3350 2150 50  0000 C CNN
F 2 "Chordata:RJ12_6P_MOLEX" H 3350 2550 50  0001 C CNN
F 3 "" H 3350 2550 50  0001 C CNN
	1    3350 2550
	1    0    0    1   
$EndComp
Text GLabel 2700 2250 0    55   Input ~ 0
SDA_5
Text GLabel 3150 2750 0    55   Input ~ 0
SCL_5
Text GLabel 3150 2550 0    55   Input ~ 0
ENABLE
Text GLabel 3150 2450 0    55   Input ~ 0
VCC
$Comp
L Conn_01x06 J6
U 1 1 5B3C3B9D
P 3350 3500
F 0 "J6" H 3350 3800 50  0000 C CNN
F 1 "GATE_6" H 3350 3100 50  0000 C CNN
F 2 "Chordata:RJ12_6P_MOLEX" H 3350 3500 50  0001 C CNN
F 3 "" H 3350 3500 50  0001 C CNN
	1    3350 3500
	1    0    0    1   
$EndComp
Text GLabel 2700 3200 0    55   Input ~ 0
SDA_6
Text GLabel 3150 3700 0    55   Input ~ 0
SCL_6
Text GLabel 3150 3500 0    55   Input ~ 0
ENABLE
Text GLabel 3150 3400 0    55   Input ~ 0
VCC
NoConn ~ 1850 6500
NoConn ~ 1850 6600
NoConn ~ 1850 6700
NoConn ~ 6600 6250
NoConn ~ 6600 6350
$Comp
L PCA9548A U1
U 1 1 5B3C7860
P 5700 2450
F 0 "U1" H 5450 3500 55  0000 C CNN
F 1 "PCA9548A" H 5500 3750 55  0000 C CNN
F 2 "Housings_SOIC:SOIC-24W_7.5x15.4mm_Pitch1.27mm" H 5650 1600 55  0001 C CNN
F 3 "" H 5650 1600 55  0001 C CNN
	1    5700 2450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 5B3C7A8B
P 5400 3900
F 0 "#PWR010" H 5400 3650 50  0001 C CNN
F 1 "GND" H 5400 3750 50  0000 C CNN
F 2 "" H 5400 3900 50  0001 C CNN
F 3 "" H 5400 3900 50  0001 C CNN
	1    5400 3900
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5B3C8096
P 4500 2800
F 0 "R1" V 4580 2800 50  0000 C CNN
F 1 "R" V 4500 2800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4430 2800 50  0001 C CNN
F 3 "" H 4500 2800 50  0001 C CNN
	1    4500 2800
	0    1    1    0   
$EndComp
Text GLabel 4200 3300 0    55   Input ~ 0
VCC
Text GLabel 5100 3000 0    55   Input ~ 0
sbc-SDA
Text GLabel 5100 3100 0    55   Input ~ 0
sbc-SCL
Text GLabel 6250 1500 2    55   Input ~ 0
SCL_1
Text GLabel 6250 1400 2    55   Input ~ 0
SDA_1
Text GLabel 6250 1750 2    55   Input ~ 0
SCL_2
Text GLabel 6250 1650 2    55   Input ~ 0
SDA_2
Text GLabel 6250 2000 2    55   Input ~ 0
SCL_3
Text GLabel 6250 1900 2    55   Input ~ 0
SDA_3
Text GLabel 6250 2150 2    55   Input ~ 0
SDA_4
Text GLabel 6250 2250 2    55   Input ~ 0
SCL_4
Text GLabel 6250 2400 2    55   Input ~ 0
SDA_5
Text GLabel 6250 2500 2    55   Input ~ 0
SCL_5
Text GLabel 6250 2650 2    55   Input ~ 0
SDA_6
Text GLabel 6250 2750 2    55   Input ~ 0
SCL_6
$Comp
L GND #PWR011
U 1 1 5B3CA0C4
P 5900 3900
F 0 "#PWR011" H 5900 3650 50  0001 C CNN
F 1 "GND" H 5900 3750 50  0000 C CNN
F 2 "" H 5900 3900 50  0001 C CNN
F 3 "" H 5900 3900 50  0001 C CNN
	1    5900 3900
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5B3CA4C7
P 4900 4250
F 0 "R2" V 4980 4250 50  0000 C CNN
F 1 "R" V 4900 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4830 4250 50  0001 C CNN
F 3 "" H 4900 4250 50  0001 C CNN
	1    4900 4250
	0    1    1    0   
$EndComp
$Comp
L R R3
U 1 1 5B3CA5AA
P 4800 4450
F 0 "R3" V 4880 4450 50  0000 C CNN
F 1 "R" V 4800 4450 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4730 4450 50  0001 C CNN
F 3 "" H 4800 4450 50  0001 C CNN
	1    4800 4450
	0    1    1    0   
$EndComp
$Comp
L TEST TP4
U 1 1 5B3CC2F2
P 2950 1300
F 0 "TP4" H 2950 1600 50  0000 C BNN
F 1 "T-G4" H 2950 1550 50  0000 C CNN
F 2 "Test-points:TestPoint_Plated_Hole_D2.0mm" H 2950 1300 50  0001 C CNN
F 3 "" H 2950 1300 50  0001 C CNN
	1    2950 1300
	1    0    0    -1  
$EndComp
$Comp
L TEST TP5
U 1 1 5B3CC532
P 2950 2250
F 0 "TP5" H 2950 2550 50  0000 C BNN
F 1 "T-G5" H 2950 2500 50  0000 C CNN
F 2 "Test-points:TestPoint_Plated_Hole_D2.0mm" H 2950 2250 50  0001 C CNN
F 3 "" H 2950 2250 50  0001 C CNN
	1    2950 2250
	1    0    0    -1  
$EndComp
$Comp
L TEST TP6
U 1 1 5B3CC5C7
P 2950 3200
F 0 "TP6" H 2950 3500 50  0000 C BNN
F 1 "T-G6" H 2950 3450 50  0000 C CNN
F 2 "Test-points:TestPoint_Plated_Hole_D2.0mm" H 2950 3200 50  0001 C CNN
F 3 "" H 2950 3200 50  0001 C CNN
	1    2950 3200
	1    0    0    -1  
$EndComp
$Comp
L TEST TP3
U 1 1 5B3CC661
P 1600 3450
F 0 "TP3" H 1600 3750 50  0000 C BNN
F 1 "T-G3" H 1600 3700 50  0000 C CNN
F 2 "Test-points:TestPoint_Plated_Hole_D2.0mm" H 1600 3450 50  0001 C CNN
F 3 "" H 1600 3450 50  0001 C CNN
	1    1600 3450
	-1   0    0    1   
$EndComp
$Comp
L TEST TP2
U 1 1 5B3CC96C
P 1600 2500
F 0 "TP2" H 1600 2800 50  0000 C BNN
F 1 "T-G2" H 1600 2750 50  0000 C CNN
F 2 "Test-points:TestPoint_Plated_Hole_D2.0mm" H 1600 2500 50  0001 C CNN
F 3 "" H 1600 2500 50  0001 C CNN
	1    1600 2500
	-1   0    0    1   
$EndComp
$Comp
L TEST TP1
U 1 1 5B3CCBBA
P 1600 1550
F 0 "TP1" H 1600 1850 50  0000 C BNN
F 1 "T-G1" H 1600 1800 50  0000 C CNN
F 2 "Test-points:TestPoint_Plated_Hole_D2.0mm" H 1600 1550 50  0001 C CNN
F 3 "" H 1600 1550 50  0001 C CNN
	1    1600 1550
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x02 J10
U 1 1 5B3CD481
P 1300 5450
F 0 "J10" H 1300 5550 50  0000 C CNN
F 1 "GPIO" H 1300 5250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1300 5450 50  0001 C CNN
F 3 "" H 1300 5450 50  0001 C CNN
	1    1300 5450
	-1   0    0    1   
$EndComp
Text GLabel 1500 5450 2    55   Input ~ 0
io_2
Text GLabel 1500 5350 2    55   Input ~ 0
io_3
NoConn ~ 6150 8050
$Comp
L PWR_FLAG #FLG012
U 1 1 5B3D3F0D
P 1400 6500
F 0 "#FLG012" H 1400 6575 50  0001 C CNN
F 1 "PWR_FLAG" H 1400 6650 50  0000 C CNN
F 2 "" H 1400 6500 50  0001 C CNN
F 3 "" H 1400 6500 50  0001 C CNN
	1    1400 6500
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG013
U 1 1 5B3D751A
P 6950 6650
F 0 "#FLG013" H 6950 6725 50  0001 C CNN
F 1 "PWR_FLAG" H 6950 6800 50  0000 C CNN
F 2 "" H 6950 6650 50  0001 C CNN
F 3 "" H 6950 6650 50  0001 C CNN
	1    6950 6650
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 5B3D2709
P 5450 8100
F 0 "D1" H 5450 8200 50  0000 C CNN
F 1 "LED" H 5450 8000 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm_FlatTop" H 5450 8100 50  0001 C CNN
F 3 "" H 5450 8100 50  0001 C CNN
	1    5450 8100
	0    -1   -1   0   
$EndComp
$Comp
L R R4
U 1 1 5B3D27F0
P 5450 8650
F 0 "R4" V 5530 8650 50  0000 C CNN
F 1 "R" V 5450 8650 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 5380 8650 50  0001 C CNN
F 3 "" H 5450 8650 50  0001 C CNN
	1    5450 8650
	-1   0    0    1   
$EndComp
Text GLabel 2750 9100 2    55   Input ~ 0
PWR-5V
NoConn ~ 6250 2900
NoConn ~ 6250 3000
NoConn ~ 6250 3150
NoConn ~ 6250 3250
Text GLabel 4700 5050 2    55   Input ~ 0
SCL_1
Text GLabel 4700 4950 2    55   Input ~ 0
SDA_1
Text GLabel 4700 5300 2    55   Input ~ 0
SCL_2
Text GLabel 4700 5200 2    55   Input ~ 0
SDA_2
Text GLabel 4700 5550 2    55   Input ~ 0
SCL_3
Text GLabel 4700 5450 2    55   Input ~ 0
SDA_3
Text GLabel 4700 5700 2    55   Input ~ 0
SDA_4
Text GLabel 4700 5800 2    55   Input ~ 0
SCL_4
Text GLabel 4700 5950 2    55   Input ~ 0
SDA_5
Text GLabel 4700 6050 2    55   Input ~ 0
SCL_5
Text GLabel 4700 6200 2    55   Input ~ 0
SDA_6
Text GLabel 4700 6300 2    55   Input ~ 0
SCL_6
$Comp
L R R16
U 1 1 5C4A096E
P 4450 6300
F 0 "R16" V 4530 6300 50  0000 C CNN
F 1 "R" V 4450 6300 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4380 6300 50  0001 C CNN
F 3 "" H 4450 6300 50  0001 C CNN
	1    4450 6300
	0    1    1    0   
$EndComp
$Comp
L R R15
U 1 1 5C4A0A96
P 4250 6200
F 0 "R15" V 4330 6200 50  0000 C CNN
F 1 "R" V 4250 6200 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4180 6200 50  0001 C CNN
F 3 "" H 4250 6200 50  0001 C CNN
	1    4250 6200
	0    1    1    0   
$EndComp
$Comp
L R R14
U 1 1 5C4A0B0F
P 4450 6050
F 0 "R14" V 4530 6050 50  0000 C CNN
F 1 "R" V 4450 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4380 6050 50  0001 C CNN
F 3 "" H 4450 6050 50  0001 C CNN
	1    4450 6050
	0    1    1    0   
$EndComp
$Comp
L R R13
U 1 1 5C4A0B8D
P 4250 5950
F 0 "R13" V 4330 5950 50  0000 C CNN
F 1 "R" V 4250 5950 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4180 5950 50  0001 C CNN
F 3 "" H 4250 5950 50  0001 C CNN
	1    4250 5950
	0    1    1    0   
$EndComp
$Comp
L R R12
U 1 1 5C4A0C0C
P 4450 5800
F 0 "R12" V 4530 5800 50  0000 C CNN
F 1 "R" V 4450 5800 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4380 5800 50  0001 C CNN
F 3 "" H 4450 5800 50  0001 C CNN
	1    4450 5800
	0    1    1    0   
$EndComp
$Comp
L R R11
U 1 1 5C4A0C98
P 4250 5700
F 0 "R11" V 4330 5700 50  0000 C CNN
F 1 "R" V 4250 5700 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4180 5700 50  0001 C CNN
F 3 "" H 4250 5700 50  0001 C CNN
	1    4250 5700
	0    1    1    0   
$EndComp
$Comp
L R R10
U 1 1 5C4A0E73
P 4450 5550
F 0 "R10" V 4530 5550 50  0000 C CNN
F 1 "R" V 4450 5550 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4380 5550 50  0001 C CNN
F 3 "" H 4450 5550 50  0001 C CNN
	1    4450 5550
	0    1    1    0   
$EndComp
$Comp
L R R9
U 1 1 5C4A0E79
P 4250 5450
F 0 "R9" V 4330 5450 50  0000 C CNN
F 1 "R" V 4250 5450 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4180 5450 50  0001 C CNN
F 3 "" H 4250 5450 50  0001 C CNN
	1    4250 5450
	0    1    1    0   
$EndComp
$Comp
L R R8
U 1 1 5C4A0E7F
P 4450 5300
F 0 "R8" V 4530 5300 50  0000 C CNN
F 1 "R" V 4450 5300 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4380 5300 50  0001 C CNN
F 3 "" H 4450 5300 50  0001 C CNN
	1    4450 5300
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 5C4A0E85
P 4250 5200
F 0 "R7" V 4330 5200 50  0000 C CNN
F 1 "R" V 4250 5200 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4180 5200 50  0001 C CNN
F 3 "" H 4250 5200 50  0001 C CNN
	1    4250 5200
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 5C4A0E8B
P 4450 5050
F 0 "R6" V 4530 5050 50  0000 C CNN
F 1 "R" V 4450 5050 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4380 5050 50  0001 C CNN
F 3 "" H 4450 5050 50  0001 C CNN
	1    4450 5050
	0    1    1    0   
$EndComp
$Comp
L R R5
U 1 1 5C4A0E91
P 4250 4950
F 0 "R5" V 4330 4950 50  0000 C CNN
F 1 "R" V 4250 4950 50  0000 C CNN
F 2 "Resistors_SMD:R_1206_HandSoldering" V 4180 4950 50  0001 C CNN
F 3 "" H 4250 4950 50  0001 C CNN
	1    4250 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	6150 4800 6050 4800
Wire Wire Line
	6150 4900 6050 4900
Wire Wire Line
	6150 5000 6050 5000
Wire Wire Line
	6150 5100 6050 5100
Wire Wire Line
	6650 4900 6750 4900
Wire Wire Line
	6650 5000 6750 5000
Wire Wire Line
	6650 5100 6750 5100
Wire Wire Line
	2150 6300 2150 6100
Wire Wire Line
	2150 6100 2500 6100
Wire Wire Line
	2500 6100 2500 7400
Wire Wire Line
	2250 6300 2250 6100
Connection ~ 2250 6100
Wire Wire Line
	6300 6650 6300 6750
Wire Wire Line
	6450 6750 6450 6950
Wire Wire Line
	6200 6650 6200 6750
Connection ~ 6300 6750
Wire Wire Line
	2200 8450 2200 9100
Wire Wire Line
	2000 8450 2000 9200
Wire Wire Line
	1250 6900 1850 6900
Connection ~ 1500 6900
Wire Wire Line
	3150 7950 3600 7950
Wire Wire Line
	3600 7000 3600 8250
Connection ~ 3600 7950
Wire Wire Line
	2500 7850 3350 7850
Wire Wire Line
	3350 7850 3350 7950
Connection ~ 3350 7950
Wire Wire Line
	3600 8550 3600 8700
Wire Wire Line
	1500 8550 1500 9200
Wire Wire Line
	3600 7000 4300 7000
Wire Wire Line
	4800 7000 5200 7000
Wire Wire Line
	4550 7100 4550 7650
Wire Wire Line
	4550 7650 5200 7650
Wire Wire Line
	5000 7500 5000 7650
Connection ~ 5000 7650
Wire Wire Line
	6350 8900 6350 9100
Wire Wire Line
	6350 9100 7100 9100
Wire Wire Line
	6150 8900 6150 9200
Wire Wire Line
	6350 8050 6350 7800
Wire Wire Line
	5450 7800 6500 7800
Wire Wire Line
	1500 6900 1500 8250
Wire Wire Line
	2500 7950 2550 7950
Wire Wire Line
	2550 7950 2550 8350
Wire Wire Line
	2550 8350 2700 8350
Wire Wire Line
	3000 8350 3150 8350
Wire Wire Line
	3150 8350 3150 7950
Wire Wire Line
	6900 9000 6900 9100
Connection ~ 6900 9100
Connection ~ 6450 6750
Wire Wire Line
	2800 5350 2600 5350
Wire Wire Line
	2800 5450 2400 5450
Wire Wire Line
	2250 3350 1450 3350
Wire Wire Line
	2250 1150 2250 4400
Wire Wire Line
	1450 3050 2250 3050
Connection ~ 2250 3350
Wire Wire Line
	2250 2400 1450 2400
Wire Wire Line
	2250 2100 1450 2100
Wire Wire Line
	1450 1450 2250 1450
Wire Wire Line
	1450 1150 2250 1150
Connection ~ 2250 1450
Connection ~ 2250 2100
Connection ~ 2250 3050
Connection ~ 2250 2400
Wire Wire Line
	3150 1400 2250 1400
Wire Wire Line
	3150 1700 2250 1700
Wire Wire Line
	2250 2350 3150 2350
Wire Wire Line
	2250 2650 3150 2650
Wire Wire Line
	3150 3300 2250 3300
Wire Wire Line
	3150 3600 2250 3600
Connection ~ 2250 1400
Connection ~ 2250 3600
Connection ~ 2250 3300
Connection ~ 2250 2650
Connection ~ 2250 2350
Connection ~ 2250 1700
Wire Wire Line
	5400 3750 5400 3900
Wire Wire Line
	5100 2800 4650 2800
Wire Wire Line
	4200 3300 5100 3300
Wire Wire Line
	4350 2800 4300 2800
Wire Wire Line
	4300 2800 4300 3300
Connection ~ 4300 3300
Wire Wire Line
	5900 3750 5900 3900
Wire Wire Line
	5700 3750 5700 4250
Wire Wire Line
	5700 4250 5050 4250
Wire Wire Line
	5800 3750 5800 4450
Wire Wire Line
	5800 4450 4950 4450
Wire Wire Line
	4750 4250 4650 4250
Wire Wire Line
	4650 4250 4650 3300
Connection ~ 4650 3300
Wire Wire Line
	4650 4450 4550 4450
Wire Wire Line
	4550 4450 4550 3300
Connection ~ 4550 3300
Wire Wire Line
	2700 1300 3150 1300
Wire Wire Line
	1450 1550 1850 1550
Wire Wire Line
	1450 2500 1850 2500
Wire Wire Line
	1450 3450 1850 3450
Wire Wire Line
	2700 3200 3150 3200
Wire Wire Line
	2700 2250 3150 2250
Connection ~ 2950 1300
Connection ~ 1600 1550
Connection ~ 2950 2250
Connection ~ 1600 2500
Connection ~ 2950 3200
Connection ~ 1600 3450
Wire Wire Line
	6200 6750 6950 6750
Wire Wire Line
	7450 4800 7450 5250
Wire Wire Line
	7200 4600 7200 4800
Connection ~ 7200 4800
Wire Wire Line
	6650 4800 7450 4800
Wire Wire Line
	1400 6500 1400 6900
Connection ~ 1400 6900
Connection ~ 1600 6900
Wire Wire Line
	6950 6750 6950 6650
Wire Wire Line
	2200 9100 2750 9100
Wire Wire Line
	5450 7800 5450 7950
Connection ~ 6350 7800
Wire Wire Line
	5450 8250 5450 8500
Wire Wire Line
	5450 8800 5450 9100
Wire Wire Line
	5450 9100 6150 9100
Connection ~ 6150 9100
Wire Wire Line
	1700 7950 1500 7950
Connection ~ 1500 7950
Wire Wire Line
	4700 4950 4400 4950
Wire Wire Line
	4600 5050 4700 5050
Wire Wire Line
	4400 5200 4700 5200
Wire Wire Line
	4600 5300 4700 5300
Wire Wire Line
	4400 5450 4700 5450
Wire Wire Line
	4600 5550 4700 5550
Wire Wire Line
	4400 5700 4700 5700
Wire Wire Line
	4600 5800 4700 5800
Wire Wire Line
	4400 5950 4700 5950
Wire Wire Line
	4600 6050 4700 6050
Wire Wire Line
	4400 6200 4700 6200
Wire Wire Line
	4600 6300 4700 6300
Wire Wire Line
	4000 6300 4300 6300
Wire Wire Line
	4000 4700 4000 6300
Wire Wire Line
	4000 4700 3650 4700
Wire Wire Line
	4100 4950 4000 4950
Connection ~ 4000 4950
Wire Wire Line
	4300 5050 4000 5050
Connection ~ 4000 5050
Wire Wire Line
	4100 5200 4000 5200
Connection ~ 4000 5200
Wire Wire Line
	4300 5300 4000 5300
Connection ~ 4000 5300
Wire Wire Line
	4100 5450 4000 5450
Connection ~ 4000 5450
Wire Wire Line
	4300 5550 4000 5550
Connection ~ 4000 5550
Wire Wire Line
	4100 5700 4000 5700
Connection ~ 4000 5700
Wire Wire Line
	4300 5800 4000 5800
Connection ~ 4000 5800
Wire Wire Line
	4100 5950 4000 5950
Connection ~ 4000 5950
Wire Wire Line
	4300 6050 4000 6050
Connection ~ 4000 6050
Wire Wire Line
	4100 6200 4000 6200
Connection ~ 4000 6200
Text GLabel 3650 4700 0    55   Input ~ 0
VCC
$EndSCHEMATC
