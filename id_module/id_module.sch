EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chordata
LIBS:id_module-cache
EELAYER 25 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title "Chordata ID-MODULE for K-CEPTOR Schematic"
Date "2017-07-25"
Rev "1"
Comp "Chordata"
Comment1 "design by Bruno laurencich"
Comment2 "http://chordata.cc"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_02X03 J1
U 1 1 59763B0C
P 3850 2500
F 0 "J1" H 3850 2700 50  0000 C CNN
F 1 "ID_INPUT" H 3850 2300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 3850 1300 50  0001 C CNN
F 3 "" H 3850 1300 50  0001 C CNN
	1    3850 2500
	1    0    0    -1  
$EndComp
Text GLabel 2650 2400 0    55   Input ~ 0
3.3v
Text GLabel 3600 2500 0    55   Input ~ 0
XORL
Text GLabel 3600 2600 0    55   Input ~ 0
SDA_T
Text GLabel 4100 2400 2    55   Input ~ 0
XORH
Text GLabel 5200 2500 2    55   Input ~ 0
GND
Text GLabel 4100 2600 2    55   Input ~ 0
SCL_T
Text GLabel 2850 4600 0    55   Input ~ 0
3.3v
Text GLabel 4900 4600 2    55   Input ~ 0
GND
Text GLabel 3800 4200 0    55   Input ~ 0
XORL
$Comp
L R R1
U 1 1 5977263E
P 3350 4600
F 0 "R1" V 3430 4600 50  0000 C CNN
F 1 "RLT" V 3350 4600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 3280 4600 50  0001 C CNN
F 3 "" H 3350 4600 50  0001 C CNN
	1    3350 4600
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 5977269D
P 4350 4600
F 0 "R2" V 4430 4600 50  0000 C CNN
F 1 "RLB" V 4350 4600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P5.08mm_Vertical" V 4280 4600 50  0001 C CNN
F 3 "" H 4350 4600 50  0001 C CNN
	1    4350 4600
	0    1    1    0   
$EndComp
Text GLabel 2850 5700 0    55   Input ~ 0
3.3v
Text GLabel 4900 5700 2    55   Input ~ 0
GND
$Comp
L R R3
U 1 1 59772968
P 3350 5700
F 0 "R3" V 3430 5700 50  0000 C CNN
F 1 "RHT" V 3350 5700 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 3280 5700 50  0001 C CNN
F 3 "" H 3350 5700 50  0001 C CNN
	1    3350 5700
	0    -1   -1   0   
$EndComp
$Comp
L R R4
U 1 1 5977296E
P 4350 5700
F 0 "R4" V 4430 5700 50  0000 C CNN
F 1 "RHB" V 4350 5700 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P5.08mm_Vertical" V 4280 5700 50  0001 C CNN
F 3 "" H 4350 5700 50  0001 C CNN
	1    4350 5700
	0    1    1    0   
$EndComp
Text GLabel 4000 5250 2    55   Input ~ 0
XORH
Wire Wire Line
	4900 4600 4500 4600
Wire Wire Line
	4200 4600 3500 4600
Wire Wire Line
	3200 4600 2850 4600
Wire Wire Line
	4900 5700 4500 5700
Wire Wire Line
	4200 5700 3500 5700
Wire Wire Line
	3200 5700 2850 5700
Wire Wire Line
	4000 5250 3850 5250
Wire Wire Line
	3850 5250 3850 5700
Connection ~ 3850 5700
Wire Wire Line
	3800 4200 3900 4200
Wire Wire Line
	3900 4200 3900 4600
Connection ~ 3900 4600
$Comp
L 24LC02 U1
U 1 1 59772D76
P 3550 7700
F 0 "U1" H 3300 7950 50  0000 C CNN
F 1 "24LC02" H 3750 7950 50  0001 C CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 3600 7450 50  0001 L CNN
F 3 "" H 3550 7600 50  0001 C CNN
	1    3550 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2400 2650 2400
Wire Wire Line
	5200 2500 4100 2500
Wire Wire Line
	3050 2400 3050 2000
Connection ~ 3050 2400
Wire Wire Line
	3350 2400 3350 2000
Connection ~ 3350 2400
Wire Wire Line
	4900 2000 4900 2900
Connection ~ 4900 2500
$Comp
L Earth #PWR01
U 1 1 597730D0
P 3550 8300
F 0 "#PWR01" H 3550 8050 50  0001 C CNN
F 1 "Earth" H 3550 8150 50  0001 C CNN
F 2 "" H 3550 8300 50  0001 C CNN
F 3 "" H 3550 8300 50  0001 C CNN
	1    3550 8300
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR02
U 1 1 5977310F
P 3050 2000
F 0 "#PWR02" H 3050 1850 50  0001 C CNN
F 1 "VCC" H 3050 2150 50  0000 C CNN
F 2 "" H 3050 2000 50  0001 C CNN
F 3 "" H 3050 2000 50  0001 C CNN
	1    3050 2000
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 5977323D
P 3350 2000
F 0 "#FLG03" H 3350 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 3350 2150 50  0000 C CNN
F 2 "" H 3350 2000 50  0001 C CNN
F 3 "" H 3350 2000 50  0001 C CNN
	1    3350 2000
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG04
U 1 1 59773347
P 4900 2000
F 0 "#FLG04" H 4900 2075 50  0001 C CNN
F 1 "PWR_FLAG" H 4900 2150 50  0000 C CNN
F 2 "" H 4900 2000 50  0001 C CNN
F 3 "" H 4900 2000 50  0001 C CNN
	1    4900 2000
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR05
U 1 1 59773881
P 3550 7100
F 0 "#PWR05" H 3550 6950 50  0001 C CNN
F 1 "VCC" H 3550 7250 50  0000 C CNN
F 2 "" H 3550 7100 50  0001 C CNN
F 3 "" H 3550 7100 50  0001 C CNN
	1    3550 7100
	1    0    0    -1  
$EndComp
$Comp
L Earth #PWR06
U 1 1 5977389E
P 4900 2900
F 0 "#PWR06" H 4900 2650 50  0001 C CNN
F 1 "Earth" H 4900 2750 50  0001 C CNN
F 2 "" H 4900 2900 50  0001 C CNN
F 3 "" H 4900 2900 50  0001 C CNN
	1    4900 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 7400 3550 7100
Wire Wire Line
	3550 8000 3550 8300
Wire Wire Line
	3150 7600 2900 7600
Wire Wire Line
	2900 7600 2900 8300
Wire Wire Line
	3150 7700 2900 7700
Connection ~ 2900 7700
Wire Wire Line
	3150 7800 2900 7800
Connection ~ 2900 7800
$Comp
L Earth #PWR07
U 1 1 59773AC5
P 2900 8300
F 0 "#PWR07" H 2900 8050 50  0001 C CNN
F 1 "Earth" H 2900 8150 50  0001 C CNN
F 2 "" H 2900 8300 50  0001 C CNN
F 3 "" H 2900 8300 50  0001 C CNN
	1    2900 8300
	1    0    0    -1  
$EndComp
Text GLabel 4450 7700 2    55   Input ~ 0
SCL_T
Text GLabel 4250 7600 2    55   Input ~ 0
SDA_T
Wire Wire Line
	4250 7600 3950 7600
Wire Wire Line
	4450 7700 3950 7700
Wire Wire Line
	3950 7800 4450 7800
Wire Wire Line
	4450 7800 4450 8300
$Comp
L Earth #PWR08
U 1 1 5B3F43C7
P 4450 8300
F 0 "#PWR08" H 4450 8050 50  0001 C CNN
F 1 "Earth" H 4450 8150 50  0001 C CNN
F 2 "" H 4450 8300 50  0001 C CNN
F 3 "" H 4450 8300 50  0001 C CNN
	1    4450 8300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
